package wallet

import com.google.gson.Gson
import spock.lang.Specification
import wallet.domain.Account
import wallet.domain.HttpResponse
import wallet.domain.TransferRequest

import static java.util.UUID.randomUUID
import static wallet.ApiTestUtils.request
import static wallet.Main.main
import static spark.Spark.awaitInitialization
import static spark.Spark.stop

class WalletE2ETest extends Specification {

    def setupSpec() {
        main()
        awaitInitialization()
    }

    def cleanupSpec() {
        stop()
    }

    def 'should add 2 new accounts, add money and successfully transfer money between them'() {
        given: 'accounts are created'
        def source = request('POST', '/account').json(Account).getAccountId()
        def destination = request('POST', '/account').json(Account).getAccountId()

        and: 'some money is added'
        request('PATCH', "/account/${source}/1000")
        request('PATCH', "/account/${destination}/800")

        when: 'transfer is committed'
        def result = request('POST', '/transfer', createTransferRequest(source, destination, 600))

        then: 'transfer is successful, account balances are changed'
        result.status == 200
        request('GET', "/account/${source}").json(Account).getBalance() == 400
        request('GET', "/account/${destination}").json(Account).getBalance() == 1400
    }

    def 'should not allow to transfer if source balance is insufficient or negative transfer is attempted'() {
        given: 'accounts are created'
        def source = request('POST', '/account').json(Account).getAccountId()
        def destination = request('POST', '/account').json(Account).getAccountId()

        and: 'some money is added'
        request('PATCH', "/account/${source}/1000")
        request('PATCH', "/account/${destination}/800")

        when: 'transfer for amount that exceeds source balance is committed'
        def result = request('POST', '/transfer', createTransferRequest(source, destination, 1600))

        then: 'transfer failed'
        result.status == 400
        result.json(HttpResponse).getMessage() == 'Insufficient balance!'

        when: 'transfer for negative amount of money is committed'
        result = request('POST', '/transfer', createTransferRequest(source, destination, -200))

        then: 'transfer failed'
        result.status == 400
        result.json(HttpResponse).getMessage() == 'Money to transfer cannot be less or equal to 0!'

        and: 'account balances remain unchanged'
        request('GET', "/account/${source}").json(Account).getBalance() == 1000
        request('GET', "/account/${destination}").json(Account).getBalance() == 800
    }

    def 'should not allow to transfer from non-existing source, to non-existing destination, or to the same account'() {
        given: 'account are created and some invalid accountId is created'
        def validAccount = request('POST', '/account').json(Account).getAccountId()
        def notValidAccount = randomUUID()

        and: 'some money is added'
        request('PATCH', "/account/${validAccount}/800")

        when: 'transfer from invalid account is committed'
        def result = request('POST', '/transfer', createTransferRequest(notValidAccount, validAccount, 1600))

        then: 'transfer failed'
        result.status == 400
        result.json(HttpResponse).getMessage() == "Account ${notValidAccount} does not exist in the database!"

        when: 'transfer to invalid account is committed is committed'
        result = request('POST', '/transfer', createTransferRequest(validAccount, notValidAccount, 1600))

        then: 'transfer failed'
        result.status == 400
        result.json(HttpResponse).getMessage() == "Account ${notValidAccount} does not exist in the database!"

        when: 'transfer from and to the same account is committed is committed'
        result = request('POST', '/transfer', createTransferRequest(validAccount, validAccount, 1600))

        then: 'transfer failed'
        result.status == 400
        result.json(HttpResponse).getMessage() == "Source and destination of the transfer must be different!"

        and: 'account balance remains unchanged'
        request('GET', "/account/${validAccount}").json(Account).getBalance() == 800
    }

    def 'should not allow malformed transfers'() {
        given: 'accounts are created'
        def source = request('POST', '/account').json(Account).getAccountId()
        def destination = request('POST', '/account').json(Account).getAccountId()

        and: 'some money is added'
        request('PATCH', "/account/${source}/1000")
        request('PATCH', "/account/${destination}/800")

        when: 'malformed transfer is committed - broken source account id'
        def result = request('POST', '/transfer', createMalformedRequest('broken source', destination, 600))

        then: 'transfer failed'
        result.status == 400
        result.json(HttpResponse).getMessage() == 'Invalid UUID string: broken source'

        when: 'malformed transfer is committed - broken destination account id'
        result = request('POST', '/transfer', createMalformedRequest('broken destination', destination, 600))

        then: 'transfer failed'
        result.status == 400
        result.json(HttpResponse).getMessage() == 'Invalid UUID string: broken destination'

        when: 'malformed transfer is committed - broken amount of money'
        result = request('POST', '/transfer', createMalformedRequest(source, destination, 'broken money'))

        then: 'transfer failed'
        result.status == 400
        result.json(HttpResponse).getMessage() == 'java.lang.NumberFormatException: For input string: "broken money"'

        and: 'account balances remain unchanged'
        request('GET', "/account/${source}").json(Account).getBalance() == 1000
        request('GET', "/account/${destination}").json(Account).getBalance() == 800
    }

    def createTransferRequest(from, to, amount) {
        new Gson().toJson(new TransferRequest(from: from, to: to, amount: amount))
    }

    def createMalformedRequest(from, to, amount) {
        new Gson().toJson([from: from, to: to, amount: amount])
    }
}
