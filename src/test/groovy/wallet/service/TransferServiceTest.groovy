package wallet.service

import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll
import wallet.domain.Account
import wallet.exceptions.AccountException

import static java.util.UUID.randomUUID

class TransferServiceTest extends Specification {

    static final A = randomUUID()
    static final B = randomUUID()

    def accountService = Spy(AccountService)

    @Subject
    def transferService = new TransferService(accountService)


    def 'should transfer money from source account to destination'() {
        given:
        def source = new Account(A, 1000)
        def destination = new Account(B, 500)
        accountService.findAccount(A) >> source
        accountService.findAccount(B) >> destination

        when:
        transferService.commitTransfer(A, B, 200)

        then:
        source.getBalance() == 800
        destination.getBalance() == 700
    }

    @Unroll
    def 'should not transfer money due to #issue'() {
        given:
        def source = new Account(A, 1000)
        def destination = new Account(B, 500)
        accountService.findAccount(A) >> source
        accountService.findAccount(B) >> destination

        when:
        transferService.commitTransfer(from, to, amount)

        then:
        def exception = thrown(exceptionType)
        exception.getMessage() == exceptionMessage

        source.getBalance() == 1000
        destination.getBalance() == 500

        where:
        issue                         | from | to   | amount || exceptionType            || exceptionMessage
        'malformed destination'       | A    | null | 100    || AccountException         || 'AccountId cannot be empty!'
        'malformed source'            | null | B    | 100    || AccountException         || 'AccountId cannot be empty!'
        'same source and destination' | A    | A    | 100    || AccountException         || 'Source and destination of the transfer must be different!'
        'negative amount'             | A    | B    | -300   || IllegalArgumentException || 'Money to transfer cannot be less or equal to 0!'
        'insufficient funds'          | A    | B    | 2000   || AccountException         || 'Insufficient balance!'
    }
}
