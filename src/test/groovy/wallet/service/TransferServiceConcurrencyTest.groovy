package wallet.service

import spock.lang.Specification
import spock.lang.Subject
import wallet.db.InMemoryAccountDB
import wallet.domain.Account

import java.util.concurrent.CountDownLatch

import static java.util.UUID.randomUUID
import static java.util.concurrent.TimeUnit.*

class TransferServiceConcurrencyTest extends Specification {

    public static final UUID A = randomUUID()
    public static final UUID B = randomUUID()
    public static final UUID C = randomUUID()
    def accountDB = new InMemoryAccountDB()
    def accountService = new AccountService(accountDB)

    @Subject
    def transferService = new TransferService(accountService)

    def 'should synchronize correctly and dont fall into deadlock'() {
        given:
        accountDB.addAccount(new Account(A, 80000))
        accountDB.addAccount(new Account(B, 50000))
        accountDB.addAccount(new Account(C, 40000))

        and:
        def launcher = new CountDownLatch(1)
        def finisher = new CountDownLatch(150)

        when:
        25.times { initializeTransferCommitter(launcher, finisher, A, B, 2) }
        25.times { initializeTransferCommitter(launcher, finisher, A, C, 3) }

        25.times { initializeTransferCommitter(launcher, finisher, B, A, 4) }
        25.times { initializeTransferCommitter(launcher, finisher, B, C, 5) }

        25.times { initializeTransferCommitter(launcher, finisher, C, A, 6) }
        25.times { initializeTransferCommitter(launcher, finisher, C, B, 7) }

        and:
        launcher.countDown()

        then:
        finisher.await(5, SECONDS)
        accountService.findAccount(A).getBalance() == 92500
        accountService.findAccount(B).getBalance() == 50000
        accountService.findAccount(C).getBalance() == 27500
    }

    def initializeTransferCommitter(CountDownLatch launcher, CountDownLatch finisher,
                                    UUID from, UUID to, Long amount) {
        Thread.start {
            launcher.await()
            try {
                100.times { transferService.commitTransfer(from, to, amount) }
            } catch (ignored) {
            } finally {
                finisher.countDown()
            }
        }
    }
}
