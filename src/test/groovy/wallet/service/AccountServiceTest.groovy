package wallet.service

import wallet.exceptions.AccountException
import spock.lang.Specification
import spock.lang.Subject
import wallet.db.AccountDB
import wallet.domain.Account

import static java.util.UUID.randomUUID

class AccountServiceTest extends Specification {

    static final ID = randomUUID()

    def accountDB = Mock(AccountDB)

    @Subject
    def accountService = new AccountService(accountDB)

    def 'should open new account'() {
        given:
        def persistedAccounts = []
        accountDB.addAccount(_ as Account) >> { List args ->
            persistedAccounts += args[0]
        }

        when: 'new account is requested'
        def account = accountService.openAccount()

        then: 'new account is successfully created and persisted'
        [account] == persistedAccounts
    }

    def 'should add more money to account'() {
        given:
        def account = new Account(ID, 1000)
        accountDB.getAccount(ID) >> account

        when: 'valid amount of money is added to the account'
        accountService.addMoneyToAccount(ID, 100)

        then: 'balance increased'
        account.getBalance() == 1100

        when: 'invalid amount of money is added to the account'
        accountService.addMoneyToAccount(ID, -300)

        then: 'balance unchanged, exception thrown with appropriate message'
        account.getBalance() == 1100
        def exception = thrown(IllegalArgumentException)
        exception.getMessage() == 'Money to add to account must be more than 0!'
    }

    def 'should find existing account'() {
        given:
        def account = new Account(ID, 1000)
        accountDB.getAccount(ID) >> account

        when: 'account is requested'
        def result = accountService.findAccount(ID)

        then: 'account has been found'
        result == account

        when: 'malformed account id is given'
        accountService.findAccount(null)

        then: 'exception with appropriate message is thrown'
        def exception = thrown(AccountException)
        exception.getMessage() == 'AccountId cannot be empty!'
    }
}
