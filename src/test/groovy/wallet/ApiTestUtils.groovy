package wallet

import com.google.gson.Gson
import spark.utils.IOUtils

class ApiTestUtils {

    static TestResponse request(String method, String path) {
        request(method, path, null)
    }

    static TestResponse request(String method, String path, String payload) {
        HttpURLConnection connection
        try {
            URL url = new URL('http://localhost:4567' + path)
            connection = (HttpURLConnection) url.openConnection()
            connection.setDoOutput(true)
            if (method == 'PATCH') {
                connection.setRequestProperty('X-HTTP-Method-Override', 'PATCH')
                connection.setRequestMethod('POST')
            } else {
                connection.setRequestMethod(method)
            }
            if (payload != null) {
                putPayloadToConnectionOutput(connection, payload)
            }
            connection.connect()
            String body = IOUtils.toString(connection.getInputStream())
            return new TestResponse(connection.getResponseCode(), body)
        } catch (IOException e) {
            if (connection != null) {
                String body = IOUtils.toString(connection.getErrorStream())
                return new TestResponse(connection.getResponseCode(), body)
            }
            e.printStackTrace()
            return null
        }
    }

    private static void putPayloadToConnectionOutput(HttpURLConnection connection, String payload) {
        def outputStream = connection.getOutputStream()
        def streamWriter = new OutputStreamWriter(outputStream)
        streamWriter.write(payload)
        streamWriter.flush()
        streamWriter.close()
        outputStream.close()
    }

    static class TestResponse {
        public final String body
        public final int status

        TestResponse(int status, String body) {
            this.status = status
            this.body = body
        }

        def <T> T json(Class<T> type) {
            return new Gson().fromJson(body, type)
        }
    }
}
