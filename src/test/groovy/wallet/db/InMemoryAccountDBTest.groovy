package wallet.db

import wallet.exceptions.AccountException
import spock.lang.Specification
import spock.lang.Subject
import wallet.domain.Account

import static java.util.UUID.randomUUID


class InMemoryAccountDBTest extends Specification {

    @Subject
    def accountDB = new InMemoryAccountDB()

    def 'should add account to db and successfully retrieve it'() {
        given:
        def accountId = randomUUID()
        def account = new Account(accountId, 1000)

        when:
        accountDB.addAccount(account)

        then:
        accountDB.getAccount(accountId) == account
    }

    def 'should throw exception if there is no account for given id'() {
        given:
        def accountId = randomUUID()

        when:
        accountDB.getAccount(accountId)

        then:
        def exception = thrown(AccountException)
        exception.getMessage() == "Account ${accountId} does not exist in the database!"

    }

    def 'should not allow adding the same account multiple times'() {
        given:
        def accountId = randomUUID()
        def account = new Account(accountId, 1000)

        when:
        accountDB.addAccount(account)
        accountDB.addAccount(account)

        then:
        def exception = thrown(AccountException)
        exception.getMessage() == 'Account already defined!'
    }

}
