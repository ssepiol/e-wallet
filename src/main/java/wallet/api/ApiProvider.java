package wallet.api;

import wallet.service.AccountService;
import wallet.domain.HttpResponse;
import wallet.domain.TransferRequest;
import wallet.service.TransferService;

import java.util.UUID;

import static wallet.api.ApiDictionary.*;
import static java.lang.String.join;
import static spark.Spark.*;
import static wallet.api.JsonUtil.*;

public class ApiProvider {
    private final AccountService accountService;
    private final TransferService transferService;

    public ApiProvider(AccountService accountService, TransferService transferService) {
        this.accountService = accountService;
        this.transferService = transferService;
    }

    public void startRestApi() {
        get(join("/", ACCOUNT, ACCOUNT_ID), (request, response) ->
                accountService.findAccount(UUID.fromString(request.params(ACCOUNT_ID))), json());

        post(join("/", ACCOUNT), (request, response) -> accountService.openAccount(), json());

        patch(join("/", ACCOUNT, ACCOUNT_ID, AMOUNT), (request, response) -> {
            accountService.addMoneyToAccount(
                    UUID.fromString(request.params(ACCOUNT_ID)),
                    Long.parseLong(request.params(AMOUNT)));
            return new HttpResponse(OK);
        }, json());

        post(join("/", TRANSFER), (request, response) -> {
            TransferRequest transfer = fromJson(request.body(), TransferRequest.class);
            transferService.commitTransfer(
                    transfer.getFrom(),
                    transfer.getTo(),
                    transfer.getAmount());
            return new HttpResponse(OK);
        }, json());

        exception(Exception.class, (exception, request, response) -> {
            response.status(400);
            response.body(toJson(new HttpResponse(exception.getMessage())));
        });
    }


}
