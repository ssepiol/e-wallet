package wallet.api;

public interface ApiDictionary {
    String ACCOUNT = "account";
    String TRANSFER = "transfer";

    String OK = "OK";

    String ACCOUNT_ID = ":accountId";
    String AMOUNT = ":amount";
}
