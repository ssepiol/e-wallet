package wallet.api;

import wallet.db.AccountDB;
import wallet.service.AccountService;
import wallet.service.TransferService;

public class ApiProviderFactory {
    public static ApiProvider createApiProvider(AccountDB accountDB) {
        AccountService accountService = new AccountService(accountDB);
        TransferService transferService = new TransferService(accountService);

        return new ApiProvider(accountService, transferService);
    }
}
