package wallet.db;

import wallet.domain.Account;

import java.util.UUID;

public interface AccountDB {
    void addAccount(Account account);

    Account getAccount(UUID account);
}
