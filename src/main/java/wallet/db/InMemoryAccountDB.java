package wallet.db;

import wallet.domain.Account;
import wallet.exceptions.AccountException;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class InMemoryAccountDB implements AccountDB {
    private final ConcurrentMap<UUID, Account> accounts = new ConcurrentHashMap<>();

    @Override
    public void addAccount(Account account) {
        Account existingAccount = accounts.putIfAbsent(account.getAccountId(), account);

        if (existingAccount != null) {
            throw new AccountException("Account already defined!");
        }
    }

    @Override
    public Account getAccount(UUID accountId) {
        Account account = accounts.get(accountId);

        if (account == null) {
            throw new AccountException("Account " + accountId + " does not exist in the database!");
        }

        return account;
    }
}
