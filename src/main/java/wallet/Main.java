package wallet;

import wallet.db.InMemoryAccountDB;

import static wallet.api.ApiProviderFactory.createApiProvider;

public class Main {

    public static void main(String[] args) {
        createApiProvider(new InMemoryAccountDB()).startRestApi();
    }
}
