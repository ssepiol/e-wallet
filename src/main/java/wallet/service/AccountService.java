package wallet.service;

import wallet.db.AccountDB;
import wallet.domain.Account;
import wallet.exceptions.AccountException;

import java.util.UUID;

import static java.util.UUID.randomUUID;

public class AccountService {

    private final AccountDB accountDB;

    public AccountService(AccountDB accountDB) {
        this.accountDB = accountDB;
    }

    public synchronized Account openAccount() {
        Account account = new Account(randomUUID());
        accountDB.addAccount(account);

        return account;
    }

    public Account findAccount(UUID accountId) {
        checkAccountId(accountId);

        return accountDB.getAccount(accountId);
    }

    public void addMoneyToAccount(UUID accountId, long amount) {
        checkAmount(amount);
        Account account = findAccount(accountId);

        synchronized (account) {
            account.add(amount);
        }
    }

    private void checkAmount(long amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Money to add to account must be more than 0!");
        }
    }

    private void checkAccountId(UUID accountId) {
        if (accountId == null) {
            throw new AccountException("AccountId cannot be empty!");
        }
    }

}