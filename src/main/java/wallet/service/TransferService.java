package wallet.service;

import wallet.domain.Account;
import wallet.exceptions.AccountException;

import java.util.Objects;
import java.util.UUID;

public class TransferService {

    private final AccountService accountService;

    public TransferService(AccountService accountService) {
        this.accountService = accountService;
    }

    public void commitTransfer(UUID sourceAccountId, UUID destinationAccountId, long amount) {
        checkAmount(amount);
        checkAccounts(sourceAccountId, destinationAccountId);

        Account sourceAccount = accountService.findAccount(sourceAccountId);
        Account destinationAccount = accountService.findAccount(destinationAccountId);

        commitTransfer(amount, sourceAccount, destinationAccount);
    }

    private void checkAmount(long amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Money to transfer cannot be less or equal to 0!");
        }
    }

    private void checkAccounts(UUID sourceAccountId, UUID destinationAccountId) {
        if (Objects.equals(sourceAccountId, destinationAccountId)) {
            throw new AccountException("Source and destination of the transfer must be different!");
        }
    }

    private void commitTransfer(long amount, final Account sourceAccount, final Account destinationAccount) {
        if (sourceAccount.compareTo(destinationAccount) > 0) {
            synchronized (sourceAccount) {
                synchronized (destinationAccount) {
                    transfer(amount, sourceAccount, destinationAccount);
                }
            }
        } else {
            synchronized (destinationAccount) {
                synchronized (sourceAccount) {
                    transfer(amount, sourceAccount, destinationAccount);
                }
            }
        }
    }

    private void transfer(long amount, Account sourceAccount, Account destinationAccount) {
        checkBalance(amount, sourceAccount);
        sourceAccount.subtract(amount);
        destinationAccount.add(amount);
    }

    private void checkBalance(long amount, Account sourceAccount) {
        if (sourceAccount.getBalance() < amount) {
            throw new AccountException("Insufficient balance!");
        }
    }
}
