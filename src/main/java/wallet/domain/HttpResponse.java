package wallet.domain;

public class HttpResponse {
    private final String message;

    public HttpResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
