package wallet.domain;


import java.util.Objects;
import java.util.UUID;

public class Account implements Comparable<Account> {
    private final UUID accountId;
    private volatile Long balance = 0L;

    public Account(UUID accountId) {
        this.accountId = accountId;
    }

    public Account(UUID accountId, long value) {
        this.balance = value;
        this.accountId = accountId;
    }

    public synchronized Long getBalance() {
        return balance;
    }

    public synchronized void add(long amount) {
        balance += amount;
    }

    public synchronized void subtract(long amount) {
        balance -= amount;
    }

    public UUID getAccountId() {
        return accountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return accountId.equals(account.accountId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId);
    }

    @Override
    public int compareTo(Account o) {
        return this.getAccountId().compareTo(o.getAccountId());
    }
}
