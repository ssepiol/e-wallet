# A RESTful API for money transfers, with backing implementation.


## Building

To build the application simply run
 > mvn clean install 

Both jar and uberjar will be created under /target directory. You can then run:
 >  java -jar target\wallet-1.0-jar-with-dependencies.jar

To start the application.

Tests will be executed during *clean install*, but they can also be explicitly executed with:
 > mvn test

## API definition

In total, there are http 4 endpoints within the api:

 - GET /account/*accountId* - gets the information about account with given *accountId*
 
 - POST /account - opens a new account and returns it's balance (0, since it's a new account) as well as Id.
 
 - PATCH /account/*accountId*/*amountOfMoney* - adds *amountOfMoney* to the account with *accountId*
 
 - POST /transfer - commits a transfer between accounts. Accepts body with 3 fields:
 
    - from - *accountId* used as source of money
 
    - to - *accountId* that will receive the money
 
    - amount - how much money will be transfered
 
    ```
      {
        "from": "40cd527c-0b35-4852-93d8-038bb4871b72",
         "to": "c2d366ca-a876-4bb5-8f30-80d578b25723",
         "amount": 15000
      }
   ```
    
## Other remarks

Money in the api is represented with the long value, which stands for the minor currency value.

For example, if you want to transfer 100 dollars, you should actually request transfer of 10000 (10 000 cents).
    